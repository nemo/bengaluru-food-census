require 'csv'
require 'set'

data = {}
locales = Set.new

Dir.glob("data/*.csv") do |file|
    date_key = File.basename file, '.csv'
    data[date_key] = {}
    puts file
    begin
        CSV.foreach(file, headers: true) do |row|
            locale = row[2]
            if locale[-11..-1] == ", Bangalore"
                locale = locale[0..-12]
            end
            locales << locale
            data[date_key][locale]||=0
            data[date_key][locale]+=1
        end    
    rescue Exception => e
        
    end 
end

locales = locales.to_a.sort
CSV.open("stats.csv", "wb") do |csv|
    csv << ["date"].concat(locales)
    data.each do |date, census|
        d = [date]
        locales.each do |l|
            locale_count = census[l]||0
            d << locale_count
        end
        csv << d
    end
end
