# bengaluru-food-census

Keep track of restaurant openings and closures in the city.

# Quirks

-   Zomato does not support HTTP/1.1, so wget can't be used.

# Tech

This project uses GNU Parallel, Ruby, Nokogiri, and curl.

# Features

-   Keep track of historical data using regularly generated CSV files
-   Does not use the API (since the rate-limit is too low at 1k/day)
    -   We need to checkout around 15k restaurant status (closed or not)
-   Keep track of whether restaurant is still alive or not
-   Tweet any restaurant closures (or any new openings)

For now, run the following command to get a diff of new restaurants not in the old listings:

`q -d , "SELECT * from ./2018-MM-DD.csv WHERE c1 not in (SELECT c1 from 2018-MM-DD.csv)"`
